module.exports = {   
  parserOptions: {
    ecmaVersion: 2018,
    sourceType: 'module'
  },
  env: {
      browser: true,
      node: true,
      es6: true
  },
  extends: "airbnb",
  rules: {
    semi: [2, "always"]
  }
};
