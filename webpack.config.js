const LiveReloadPlugin = require('webpack-livereload-plugin');

module.exports = {
  entry: './src/app/index.js',
  output: {
    filename: 'bundle.js',
  },
  devServer: {
    index: 'index.html',
    watchContentBase: true,
  },
  watch: true,
  module: {
    rules: [
      {
      	test: /\.js?$/,
      	include: /\/frontend/,
      	loader: "babel-loader",
            options: {
                presets: [['es2015', {modules: false}], "es2016", "es2017", "es2018"],
                plugins: ['transform-runtime'], 
            }
      },
      {
        test: /\.scss$/,
        use: [{
          loader: 'style-loader',
        }, {
          loader: 'css-loader',
        }, {
          loader: 'sass-loader',
          options: {
            includePaths: [],
          },
        }],
      }],
  },
  plugins: [
    new LiveReloadPlugin({}),
  ],
};
