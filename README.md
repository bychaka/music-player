# Music player

This is HTML5 audio player with playlist. You need to copy some url of mp3/FLAC/wav/ogg/vorbis/AAC file into the field end click play. It should be a url string, for example:

`https://cdndl.zaycev.net/113378/856875/bi-2_-_bolshie_goroda_%28zaycev.net%29.mp3`

## Install project

- Install [Node.js](https://nodejs.org/en/)
- Run `npm install` in command line

## Development server

Run `npm run start` for a dev server. Navigate to `http://localhost:8080/`. The app will automatically reload if you change any of the source files.
