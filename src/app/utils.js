function getByQuery(elem) {
  return typeof elem === 'string' ? document.querySelector(elem) : elem;
}

function prettifyTime(time) {
  let minutes = Math.floor(((time % 3600) / 60));
  let seconds = Math.floor((time % 60));

  return `${parseInt(minutes / 10)}${minutes % 10}:${parseInt(seconds / 10)}${seconds % 10}`;
}

function createElem(config) {
  let element = document.createElement(config.type);

  config.class && (element.className = config.class);
  config.id && (element.id = config.id);
  config.textContent && (element.textContent = config.textContent);
  config.handlers && 
    Object.keys(config.handlers).length &&
    Object.keys(config.handlers).forEach(key => {
      element.addEventListener(key, config.handlers[key])
    })
  config.appendTo && config.appendTo.appendChild(element);

  return element;
}

function parseStorage(){
  let jsonPlaylist = localStorage.getItem('tracks');
  let parsedPlaylist;
  if (jsonPlaylist){
    parsedPlaylist = JSON.parse(jsonPlaylist);
  }
  else {
    console.warn('playlist not found in localStorage');
    getByQuery('.progress_bar_title').textContent = 'playlist not found in localStorage';
    parsedPlaylist = [];
  }
  return parsedPlaylist;
}

module.exports =  {getByQuery, prettifyTime, createElem, parseStorage};