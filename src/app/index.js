import '../assets/styles/main.scss';
import Player from '../app/player';
import {parseStorage, getByQuery} from '../app/utils';

window.addEventListener('DOMContentLoaded', initHandlers);

function initHandlers() {

  let tracks = parseStorage();
  let player = new Player(tracks);
  player.init();
  getByQuery('.player .controls .play_pause').addEventListener('click', player.play.bind(player));
  getByQuery('.player .controls .navigation_prev').addEventListener('click', player.playPrev.bind(player));
  getByQuery('.player .controls .navigation_next').addEventListener('click', player.playNext.bind(player));
  getByQuery('.player .controls .progress_bar_stripe').addEventListener('click', player.pickNewProgress.bind(player));
  getByQuery('.add_track_btn').addEventListener('click', player.addUrlToList.bind(player));
  getByQuery('.add_track_url').addEventListener('keydown', (e)=>{
    if (e.key === 'Enter') {
      player.addUrlToList();
    }
  });
  getByQuery('.clear_playlist_btn').addEventListener('click', player.clearPlaylist.bind(player));
}