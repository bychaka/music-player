import { isNumber } from 'util';
import { getByQuery, prettifyTime, createElem } from '../app/utils';

export default class Player {
  constructor(files) {
    this.current = null;
    this.status = 'pause';
    this.progressTimeout = null;
    this.files = files.map(name => ({ name }));
  }

  init() {

    this.files.forEach((file, index) => {
      this.createPlaylistElement(file.name,index)
    });

  }

  createPlaylistElement(name, trackIndex){
      let playlist = getByQuery('.playlist');
      let playlistFileContainer = createElem({
        type: 'div',
        appendTo: playlist,
        class: 'fileEntity',
        handlers: {
          click: this.play.bind(this, null, trackIndex)
        }
      });
      createElem({
        type: 'div',
        appendTo: playlistFileContainer,
        textContent: name,
        class: 'fileEntity_title',
      });
      createElem({
        type: 'div',
        appendTo: playlistFileContainer,
        textContent: '--:--',
        class: 'fileEntity_duration',
      })
  }

  loadFile(i) {
    let f = this.files[i];
    f.file = new Audio(f.name);
    f.file.addEventListener('loadedmetadata', () => {
      getByQuery('.playlist').children[i].children[1].textContent = prettifyTime(f.file.duration);
    });

    f.file.addEventListener('ended', this.playNext.bind(this, null, i));
  }

  play(e, i = this.current || 0) {
    if (!this.files[i].file) {
      this.loadFile(i);
    } 

    let action = 'play';

    if (this.current === i) {
      action = this.status === 'pause' ? 'play' : 'pause';
      this.toggleStyles(action, i);
    } else if (typeof this.current !== 'object') {
      this.files[this.current].file.pause();
      this.files[this.current].file.currentTime = 0;
      this.toggleStyles(action, this.current, i);
    } else {
      this.toggleStyles(action, i);
    }

    this.current = i;
    this.status = action;
    this.files[i].file[action]();

    if (action === 'play') {
      this.setTitle(this.files[i].name);
      this.stopProgress();
      this.runProgress();
    } else {
      this.stopProgress();
    }
  }

  playNext(e, currentIndex) {
    let nextIndex = (currentIndex ? currentIndex : this.current) + 1;

    if (!this.files[nextIndex]) {
      nextIndex = 0;
    }

    this.play(null, nextIndex);
  }

  playPrev(e, currentIndex) {
    let prevIndex = (currentIndex ? currentIndex : this.current) - 1;

    if (!this.files[prevIndex]) {
      prevIndex = this.files.length - 1;
    }

    this.play(null, prevIndex);
  }

  setTitle(title) {
    getByQuery('.progress_bar_title').textContent = title;
  }

  setProgress(setCurrentTime, percent) {
    getByQuery('.progress_bar_container_percentage').style.width = `${percent}%`;
    setCurrentTime && setCurrentTime();
  }

  countProgress() {
    let file = this.files[this.current].file;

    return (file.currentTime * 100 / file.duration) || 0;
  }

  runProgress(percent = 0) {
    let percentage = percent || this.countProgress();
    let setCurrentTime = percent ? () => {
      this.files[this.current].file.currentTime = percentage * this.files[this.current].file.duration / 100;
    } : null;

    this.setProgress(setCurrentTime, percentage);
    this.progressTimeout = setTimeout(this.runProgress.bind(this), 1000)
  }

  stopProgress() {
    clearTimeout(this.progressTimeout);
    this.progressTimeout = null;
  }

  pickNewProgress(e) {
    if (this.status != 'play') {
      this.play();
    }

    let coords = e.target.getBoundingClientRect().left;
    let progressBar = getByQuery('.progress_bar_stripe');
    let newPercent = (e.clientX - coords) / progressBar.offsetWidth * 100;

    this.stopProgress();
    this.runProgress(newPercent);
  }

  toggleStyles(action, prev, next) {
    let playListNode = getByQuery('.playlist');
    let prevNode = (playListNode.children[prev] || {});
    let nextNode = (playListNode.children[next] || {});
    let playPause = getByQuery('.play_pause .play_pause_icon');

    if (!next && next !== 0) {
      if (!prevNode.classList.contains('fileEntity-active')) {
        prevNode.classList.add('fileEntity-active');
      }
      playPause.classList.toggle('play_pause-play');
      playPause.classList.toggle('play_pause-pause');
    } else {
      prevNode.classList.toggle('fileEntity-active');
      nextNode.classList.toggle('fileEntity-active');
    }

    if (playPause.classList.contains('play_pause-play') && action === 'play' && prev != next) {
      playPause.classList.toggle('play_pause-play');
      playPause.classList.toggle('play_pause-pause');
    }
  }

  addUrlToList(){
    let urlField = getByQuery('.add_track_url');

    if (urlField.value !== '') {    
      this.files.push({name: urlField.value});
      this.createPlaylistElement(urlField.value, (this.files.length-1));
      this.addTrackToStorage(urlField.value);
    }
    urlField.value = null;

  }

  updatePlaylist(){
    let playlist = getByQuery('.playlist');

    while (playlist.firstChild) {
      playlist.removeChild(playlist.firstChild);
    }
}

  addTrackToStorage(url){
    let localPlaylist = localStorage.getItem('tracks');
    let parsedPlaylist = [];

    if (!localStorage.getItem('tracks')) {
      console.warn('playlist not found!');      
      parsedPlaylist.push(url);
      localStorage.setItem('tracks', JSON.stringify(parsedPlaylist));
    }
    else {
      parsedPlaylist = JSON.parse(localPlaylist);
      parsedPlaylist.push(url);
      localStorage.setItem('tracks', JSON.stringify(parsedPlaylist));
    }
  }

  clearPlaylist(){
    if (localStorage.getItem('tracks')) {
      localStorage.removeItem('tracks');

      if (isNumber(this.current)) {
        this.play(null, this.current);
        this.files[this.current].file.currentTime = 0;
      }

      this.updatePlaylist();
      this.current = null;
      getByQuery('.progress_bar_container_percentage').style.width = `0%`;
      this.files = [];
      getByQuery('.progress_bar_title').textContent = 'playlist cleared';
    } 
    else {
      getByQuery('.progress_bar_title').textContent = 'playlist is already empty';
    }
  }
}